#coding:utf-8
from gifcmongo import gif_mongomanager as mongoManager
import datetime
import pymongo
import os

gif_db = mongoManager.db[mongoManager.COL_GIFDATAS]


class GifData():

    def __init__(self, id,local_path, web_path, text, duration,source='NEIHAN', isrelease=False, isDownload=False,   createTime=datetime.datetime.now(), info=None):
        self.id = id
        self.source = source
        self.local_path = local_path
        self.web_path = web_path
        self.text = text
        self.duration = duration
        self.createTime = createTime
        self.isrelease = isrelease
        self.isDownload = isDownload
        self.infos = info
        pass

    def inserOrUpdate(self):
        data = self.__dict__
        tmp = {}
        for k, v in data.items():
            tmp[k] = v
        update_set = {'$set': tmp}
        mongoManager.db[mongoManager.COL_GIFDATAS].update_one({'id': self.id}, update_set, upsert=True)

    @classmethod
    def query_unrelease_gifs(cls):
        r = mongoManager.db[mongoManager.COL_GIFDATAS].find({'isrelease':False}).sort([('createTime', pymongo.DESCENDING), ('duration', pymongo.DESCENDING)])
        if r.count():
            l = list(r)
            return l
        return []

    @classmethod
    def query_is_release(cls, id):
        id = str(id)
        count = gif_db.find({'isrelease': True, 'id': id}).count()
        return count != 0

    @classmethod
    def query_unrelease_total_time(cls):
        try:
            cursor = gif_db.aggregate(pipeline=[{'$group':{'_id' : "$isrelease", 'total' : {'$sum': '$duration'}}}])
            result = list(cursor)
            for item in result:
                # 获取没有release的总时间
                if not item['_id']:
                    return item['total']
            return 0
        except Exception as e:
            return 0

    @classmethod
    def update_release_state(cls, id, is_release=True):
        result = gif_db.update_one({'id': id}, {'$set': {'isrelease':is_release}})
        return result.modified_count == 1

    @classmethod
    def remove_files_release(cls):
        try:
            cursor = gif_db.find({'isrelease': True})
            r = list(cursor)
            for item in r:
                local_path = item['local_path']
                if os.path.exists(local_path):
                    file_dir = os.path.split(local_path)[0]
                    file_name = os.path.split(local_path)[1]
                    simple_file_name = os.path.splitext(file_name)[0]
                    gif_path = os.path.join(file_dir, simple_file_name+'.gif')
                    if os.path.exists(gif_path):
                        os.remove(gif_path)
                    os.remove(local_path)
        except Exception as e:
            print ('Exception: remove Files Release Failed')


# GifData.query_is_release('93424250383')
# GifData.query_unrelease_total_time()
# GifData.query_last_dispublish_gif()
# GifData.update_release_state('93814648604', True)

# gifData = GifData(id='1',local_path=None,
#                   web_path=None,
#                   text='ggg',
#                   duration=0,
#                   source='NEIHAN',
#                   isrelease=True,
#                   isDownload=True,
#                   createTime=datetime.datetime.now(),
#                   info=None)
# gifData.inserOrUpdate()


# GifData.remove_files_release()