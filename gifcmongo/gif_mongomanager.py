#!/usr/bin/python
#coding:utf8
import pymongo

client = pymongo.MongoClient("localhost", 27017)
# 数据库
db = client['gifc']

# 基本数据
COL_GIFDATAS = 'gif_datas'


db_collections_names = [COL_GIFDATAS]

exit_collections = db.collection_names()

for name in db_collections_names:
    if name not in exit_collections:
        db.create_collection(name=name)