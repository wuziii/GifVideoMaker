#!/usr/bin/python
#coding:utf-8

import requests
from datetime import datetime, timedelta
import time
import os
from lang import translate
import json
from gif_logger import logger as gLogger
from gif_data import GifData
import video_handler as videoHandler




config = {"lang": "zh-tw"}
with open('./config.json') as f:
    config = json.load(f)


def downloadImg(url, savepath):
    try:
        response = requests.get(url)
        if response.status_code == 200:
            gLogger.info('下载 URL %s 成功' % url.encode('utf8'))
            with open(savepath, 'wb') as f:
                f.write(response.content)
                return True
        else:
            gLogger.error('下载 URL %s 失败' % url.encode('utf8'))
            return False
    except Exception as e:
        gLogger.error('下载GIF失败!休息个5秒后继续下载!')
        time.sleep(5)
        return False


# def collect_gifs(save_dir, target_duation=0):
#     if not target_duation:
#         raise Exception('Target Duration should not be empty!')
#     collect_finish = False
#     gif_infos =[]
#     duration_range = [5, 45]
#     total_duation = 0
#     delta = 0
#     now = datetime.now()
#     while not collect_finish:
#         # 50 分钟前
#         request_date_time = now - timedelta(minutes=50 * delta)
#         request_time_stamp = time.mktime(request_date_time.timetuple())
#         try:
#             # pieces, collect_finish, cur_total_duation = collect_gif(int(request_time_stamp),
#             #                                                         save_dir,
#             #                                                         duration_range,
#             #                                                         target_duation,
#             #                                                         cur_total_duation=total_duation)
#             total_duation = cur_total_duation
#             delta += 1
#             gif_infos.extend(pieces)
#         except Exception as e:
#             gLogger.error(e)
#             gLogger.error('获取GIF 数据集 超时! 休息个10秒后继续拉数据')
#             time.sleep(10)
#
#     return gif_infos


def collect_gifs_in_seconds(save_dir, seconds):
    now = datetime.now()
    duration_range = [5, 45]
    total_duation = 0
    delta = 0
    target_duation = 1000
    isContinue = True
    while isContinue:
        request_date_time = now - timedelta(minutes=50 * delta)
        request_time_stamp = time.mktime(request_date_time.timetuple())
        try:
            collect_gif(int(request_time_stamp), save_dir)
            delta += 1
        except Exception as e:
            gLogger.error(e)
            gLogger.error('获取GIF 数据集 超时! 休息个10秒后继续拉数据')
            time.sleep(10)
        finally:
            total_sec = (datetime.now() - now).total_seconds()
            isContinue = seconds > total_sec


def collect_gif(time_stamp, save_dir, min_duration=5, max_duration=20):
    if not os.path.exists(save_dir):
        raise Exception('MP4 DIR NOT EXIT!! Could not save!')
    # time_stamp = int(time.time())
    gLogger.info('5秒后获取新时间戳的GIF')
    time.sleep(5)
    neihan_url = 'http://www.neihanshequ.com/pic/?is_json=1&app_name=neihanshequ_web&max_time=%s' % time_stamp
    r = requests.get(neihan_url, timeout=5)
    gif_infos = []
    if r.status_code == 200:
        datas = r.json()['data']['data']
        for data in datas:
            group = data['group']
            display_time = data['display_time']
            comments = data['comments']
            if 'is_gif' in group.keys() and group['is_gif']:
                interesting_comment = comments[0]['text'] if len(comments) else ''
                interesting_comment = translate('%s' % interesting_comment.encode('utf8').replace(':','→'), to_lang=config['lang'])
                digg_count = group['digg_count']
                favorite_count = group['favorite_count']
                create_time = group['create_time']
                repin_count = group['repin_count']
                share_count = group['share_count']
                comment_count = group['comment_count']
                text = group['text']

                user = group['user']
                name = user['name']

                id = group['id_str']

                is_gif = group['is_gif']
                if not is_gif:
                    gLogger.info('不是Gif')
                    continue

                local_mp4_name = '%s.mp4' % id

                if GifData.query_is_release(id):
                    gLogger.info('MP4已经发布，不下载')
                    continue

                if os.path.exists(os.path.join(save_dir, local_mp4_name)):
                    gLogger.info('本地MP4存在，不下载')
                    continue

                large_image = group['large_image']
                height = large_image['height']
                width = large_image['width']
                url = large_image['url_list'][0]['url']

                local_mp4_path = '%s%s' % (save_dir, local_mp4_name)
                download_success = downloadImg(url, local_mp4_path)
                gLogger.info('下载 %s 成功' % local_mp4_name)
                if download_success:
                    duration = videoHandler.gif_duration(local_mp4_path)/1000.0
                    if duration > max_duration or duration < min_duration:
                        gLogger.error('%s 长度不符合，不存储到数据库' % local_mp4_name)
                        continue
                    info = {
                        'display_time': display_time,
                        'digg_count': digg_count,
                        'comments': comments,
                        'interesting_comment': interesting_comment,
                        'favorite_count': favorite_count,
                        'repin_count': repin_count,
                        'share_count': share_count,
                        'comment_count': comment_count,
                        'text': text,
                        'name': name,
                        'duration': duration,
                        'height': height,
                        'width': width,
                        'url': url,
                        'local_mp4_path': local_mp4_path
                    }


                    gif_item = GifData(id=id,
                                       local_path=local_mp4_path,
                                       web_path=url,
                                       text=interesting_comment,
                                       duration=duration,
                                       isrelease=False,
                                       isDownload=download_success,
                                       info = str(info)
                                       )
                    gif_item.inserOrUpdate()
                    time.sleep(1)
                    # gLogger.info('1秒后下载')
                    gif_infos.append(info)
    else:
        gLogger.info('获取数据失败')


# collect_gif()
# collect_gifs('./source/mp4/', target_duation=120)
# collect_gifs_in_seconds('./source/mp4/', 300)